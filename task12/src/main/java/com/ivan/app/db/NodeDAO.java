package com.ivan.app.db;


import org.openstreetmap.osm._0.Node;
import org.openstreetmap.osm._0.Tag;
import org.postgresql.util.PGobject;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.StringJoiner;


/**
 * Created by ivan on 4/14/16.
 */
public class NodeDAO implements DAO<Node, Long> {
    private static String TABLE_NAME = "nodes";
    private DataSource dataSource;

    private static final String INSERT_SQL =
            "INSERT INTO " + TABLE_NAME +
                    "(id, n_lat, n_lon, n_user, n_uid, n_visible, " +
                    "n_version, n_changeset, n_timestamp, n_tags) " +
                    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    public NodeDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void addBatch(List<Node> entities) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SQL);
                for (Node entity : entities) {
                    setNode(preparedStatement, entity);
                    preparedStatement.addBatch();
                }
                preparedStatement.executeBatch();
            } catch (SQLException ex) {
                connection.rollback();
                connection.setAutoCommit(true);
                throw ex;
            }

            connection.commit();
            connection.setAutoCommit(true);
        }
    }

    @Override
    public Long create(Node entity) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
            setNode(preparedStatement, entity);
            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getLong(1);
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
        }
    }

    @Override
    public Node getById(Long id) throws SQLException {
        return null;
    }

    @Override
    public List<Node> getAll() throws SQLException {
        return null;
    }

    @Override
    public boolean delete(Long id) throws SQLException {
        return false;
    }

    @Override
    public boolean update(Node entity) throws SQLException {
        return false;
    }

    private void setNode(PreparedStatement preparedStatement, Node entity) throws SQLException {
        PGobject tagsHstore = getTagsHstoreObject(entity.getTag());
        preparedStatement.setLong(1, entity.getId().longValue());
        preparedStatement.setDouble(2, entity.getLat());
        preparedStatement.setDouble(3, entity.getLon());
        preparedStatement.setString(4, entity.getUser());
        preparedStatement.setLong(5, entity.getUid().longValue());
        if (entity.isVisible() == null) {
            preparedStatement.setNull(6, Types.BOOLEAN);
        } else {
            preparedStatement.setObject(6, entity.isVisible());
        }
        preparedStatement.setLong(7, entity.getVersion().longValue());
        preparedStatement.setLong(8, entity.getChangeset().longValue());
        preparedStatement.setTimestamp(9, new Timestamp(entity.getTimestamp().toGregorianCalendar().getTimeInMillis()));
        preparedStatement.setObject(10, tagsHstore);

    }

    private PGobject getTagsHstoreObject(List<Tag> tags) throws SQLException {
        PGobject tagsHstore = new PGobject();
        tagsHstore.setType("hstore");

        StringJoiner sj = new StringJoiner(",");

        for (Tag t : tags) {
            String k = t.getK().replace("\"", "\\\"").replace("'", "\\\'");
            String v = t.getV().replace("\"", "\\\"").replace("'", "\\\'");
            sj.add("\"" + k + "\"" + "=>" + "\"" + v + "\"");
        }
        tagsHstore.setValue(sj.toString());
        return tagsHstore;
    }
}
