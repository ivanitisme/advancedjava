package com.ivan.app.db;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by ivan on 4/15/16.
 */

public interface DAO<T, K> {

    void addBatch(List<T> entities) throws SQLException;

    K create(T entity) throws SQLException;

    T getById(K id) throws SQLException;

    List<T> getAll() throws SQLException;

    boolean delete(K id) throws SQLException;

    boolean update(T entity) throws SQLException;

}
