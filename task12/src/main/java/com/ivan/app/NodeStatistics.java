package com.ivan.app;


import org.openstreetmap.osm._0.Node;
import org.openstreetmap.osm._0.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ivan on 5/21/16.
 */
public class NodeStatistics {
    private static final Logger logger = LoggerFactory.getLogger(NodeStatistics.class);
    private Map<String, Integer> users;
    private Map<String, Integer> keys;

    public NodeStatistics() {
        users = new HashMap<>();
        keys = new HashMap<>();
    }

    public void add(Node node) {
        String name = node.getUser();

        Integer count = users.get(name);
        if (count == null) {
            users.put(name, 1);
        } else {
            users.put(name, count + 1);
        }

        for (Tag t : node.getTag()) {
            String key = t.getK();
            count = keys.get(key);
            if (count == null) {
                keys.put(key, 1);
            } else {
                keys.put(key, count + 1);
            }
        }
    }

    public Map<String, Integer> getUsersStatistic() {
        return users;
    }

    public Map<String, Integer> getKeysStatistic() {
        return keys;
    }
}
