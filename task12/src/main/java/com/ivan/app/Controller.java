package com.ivan.app;

import com.ivan.app.db.NodeDAO;
import org.openstreetmap.osm._0.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ivan on 5/21/16.
 */
public class Controller {
    private static final Logger logger = LoggerFactory.getLogger(Controller.class);
    private static final int BATCH_COUNT = 5000;
    private final XMLNodeReader xmlNodeReader;
    private final DataSource source;
    private NodeStatistics nodeStatistics;

    public Controller(XMLNodeReader xmlNodeReader, DataSource source) {
        this.xmlNodeReader = xmlNodeReader;
        this.source = source;
    }

    public void XmlToDatabase() throws JAXBException, XMLStreamException, SQLException {
        Node node;
        NodeDAO nodeDAO = new NodeDAO(source);
        List<Node> nodesToBatch = new ArrayList<>();
        int nodeCount = 0;

        nodeStatistics = new NodeStatistics();
        logger.info("Start reading xml");
        while ((node = xmlNodeReader.getNextNode()) != null) {
            nodeStatistics.add(node);
            nodesToBatch.add(node);
            if (++nodeCount == BATCH_COUNT) {
                nodeDAO.addBatch(nodesToBatch);
                nodesToBatch.clear();
                nodeCount = 0;
            }
        }
        printStatistic();
    }

    private void printStatistic() {
        nodeStatistics.getUsersStatistic().entrySet()
                .stream()
                .sorted((o1, o2) -> o2.getValue().compareTo(o1.getValue()))
                .forEach(entry -> System.out.println(entry.getKey() + " : " + entry.getValue()));
        System.out.println("***********************************");
        nodeStatistics.getKeysStatistic().entrySet()
                .stream()
                .sorted((o1, o2) -> o2.getValue().compareTo(o1.getValue()))
                .forEach(entry -> System.out.println(entry.getKey() + " : " + entry.getValue()));
    }
}
