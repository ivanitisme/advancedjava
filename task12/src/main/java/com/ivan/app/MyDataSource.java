package com.ivan.app;

import org.postgresql.ds.PGPoolingDataSource;
import org.postgresql.ds.PGSimpleDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;

/**
 * Created by ivan on 5/20/16.
 */
public class MyDataSource {
    private static final Logger logger = LoggerFactory.getLogger(MyDataSource.class);
    private static PGPoolingDataSource source;

    public synchronized static DataSource getDataSource() {
        if (source == null) {

            logger.info("Data source initialization");

            source = new PGPoolingDataSource();
            source.setServerName("localhost");
            source.setDatabaseName("ivan");
            source.setUser("postgres");
            source.setPassword("12345");
            return source;
        } else {
            return source;
        }
    }
}
