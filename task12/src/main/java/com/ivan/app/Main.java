package com.ivan.app;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.sql.DataSource;
import javax.xml.bind.JAXBException;
import javax.xml.stream.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;

/**
 * Created by ivan on 4/1/16.
 */


public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException, XMLStreamException, SAXException, JAXBException, ClassNotFoundException, SQLException {
        logger.info("Start application");
        FileInputStream fileInputStream = new FileInputStream("src/main/resources/RU-NVS.osm.bz2");
        BZip2CompressorInputStream zipStream = new BZip2CompressorInputStream(fileInputStream);
        XMLNodeReader nodeReader = new XMLNodeReader(zipStream);

        DataSource source = MyDataSource.getDataSource();
        Controller controller = new Controller(nodeReader, source);
        controller.XmlToDatabase();
    }
}

