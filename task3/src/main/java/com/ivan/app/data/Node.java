package com.ivan.app.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ivan.app.jpa.HstoreType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ivan on 02.06.2016.
 */
@Entity
@Table(name = "node")
@TypeDef(name = "hstore", typeClass = HstoreType.class)
public class Node implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "n_id")
    private BigInteger nodeId;
    @Column(name = "n_lat")
    private double lat;
    @Column(name = "n_lon")
    private double lon;
    @Column(name = "n_user")
    private String user;
    @Column(name = "n_uid")
    private BigInteger uid;
    @Column(name = "n_visible")
    @JsonIgnore
    private Boolean visible;
    @Column(name = "n_version")
    private BigInteger version;
    @Column(name = "n_changeset")
    private BigInteger changeset;
    @Column(name = "n_timestamp")
    private Timestamp timestamp;
    @Column(name = "n_tags", columnDefinition = "hstore")
    @Type(type = "hstore")
    private Map<String, String> tags;

    public Node() {
    }

    public Node(org.openstreetmap.osm._0.Node node) {
        this.nodeId = node.getId();
        this.lat = node.getLat();
        this.lon = node.getLon();
        this.user = node.getUser();
        this.uid = node.getUid();
        this.visible = node.isVisible();
        this.changeset = node.getChangeset();
        this.timestamp = new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis());
        Map<String, String> tags = new HashMap<>();
        for (org.openstreetmap.osm._0.Tag t : node.getTag()) {
            tags.put(t.getK(), t.getV());
        }
        this.tags = tags;
    }

    public Long getId() {
        return id;
    }

    public BigInteger getNodeId() {
        return nodeId;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getUser() {
        return user;
    }

    public BigInteger getUid() {
        return uid;
    }

    public boolean isVisible() {
        return visible;
    }

    public BigInteger getVersion() {
        return version;
    }

    public BigInteger getChangeset() {
        return changeset;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public Map<String, String> getTags() {
        return tags;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNodeId(BigInteger nodeId) {
        this.nodeId = nodeId;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setUid(BigInteger uid) {
        this.uid = uid;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setVersion(BigInteger version) {
        this.version = version;
    }

    public void setChangeset(BigInteger changeset) {
        this.changeset = changeset;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public void setTags(Map<String, String> tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "Node{" +
                "id=" + id +
                ", nodeId=" + nodeId +
                ", lat=" + lat +
                ", lon=" + lon +
                ", user='" + user + '\'' +
                ", uid=" + uid +
                ", visible=" + visible +
                ", version=" + version +
                ", changeset=" + changeset +
                ", timestamp=" + timestamp +
                ", tags=" + tags +
                '}';
    }
}
