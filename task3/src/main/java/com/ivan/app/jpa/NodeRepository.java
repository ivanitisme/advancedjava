package com.ivan.app.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ivan.app.data.Node;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by ivan on 02.06.2016.
 */
public interface NodeRepository extends JpaRepository<Node, Long> {
    @Query(value = "SELECT * FROM node " +
            "WHERE earth_box(ll_to_earth(:lat, :lon), :radius) @> ll_to_earth(node.n_lat, node.n_lon) " +
            "ORDER BY earth_distance(ll_to_earth(:lat, :lon), ll_to_earth(node.n_lat, node.n_lon))", nativeQuery = true)
    List<Node> getNodesInRadius(@Param("lat") double lat, @Param("lon") double lon, @Param("radius") double radius);
}
