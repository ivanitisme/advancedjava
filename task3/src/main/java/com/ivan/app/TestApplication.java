package com.ivan.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


/**
 * Created by ivan on 02.06.2016.
 */

@SpringBootApplication
public class TestApplication {

    @Bean
    @Autowired
    CommandLineRunner init(Controller controller) {
        return args -> {
//            controller.xmlToDatabase();
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }
}
