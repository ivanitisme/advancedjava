package com.ivan.app;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by ivan on 03.06.2016.
 */
@Configuration
public class AppConfiguration {
    @Bean
    public XMLNodeReader nodeReader() throws IOException, JAXBException, XMLStreamException {
        FileInputStream fileInputStream = new FileInputStream("src/main/resources/RU-NVS.osm.bz2");
        BZip2CompressorInputStream zipStream = new BZip2CompressorInputStream(fileInputStream);
        return new XMLNodeReader(zipStream);
    }

}
