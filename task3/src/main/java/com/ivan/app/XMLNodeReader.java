package com.ivan.app;

import org.openstreetmap.osm._0.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.InputStream;

/**
 * Created by ivan on 4/8/16.
 */

public class XMLNodeReader {
    private static final Logger logger = LoggerFactory.getLogger(XMLNodeReader.class);
    private XMLStreamReader streamReader;
    private Unmarshaller unmarshaller;

    public XMLNodeReader() {
    }

    public XMLNodeReader(InputStream inputStream) throws XMLStreamException, JAXBException {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        streamReader = factory.createXMLStreamReader(inputStream);
        streamReader = new NamespaceAddingReader(streamReader);
        JAXBContext jaxbContext = JAXBContext.newInstance(Node.class);
        unmarshaller = jaxbContext.createUnmarshaller();

    }

    public Node getNextNode() throws XMLStreamException, JAXBException {
        while (streamReader.hasNext()) {
            if (streamReader.isStartElement() && "node".equals(streamReader.getName().toString())) {
                Node node = (Node) unmarshaller.unmarshal(streamReader);
                return node;
            }
            streamReader.next();
        }
        return null;
    }

    private static class NamespaceAddingReader extends StreamReaderDelegate {

        private static final String NAMESPACE_URI = "http://openstreetmap.org/osm/0.6";

        public NamespaceAddingReader(XMLStreamReader reader) {
            super(reader);
        }

        @Override
        public String getNamespaceURI() {
            return NAMESPACE_URI;
        }

    }

}
