package com.ivan.app;

import com.ivan.app.jpa.NodeRepository;
import org.openstreetmap.osm._0.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ivan on 5/21/16.
 */
@Component
public class Controller {
    private static final Logger logger = LoggerFactory.getLogger(Controller.class);
    private static final int BATCH_COUNT = 5000;
    private XMLNodeReader xmlNodeReader;
    private NodeRepository repository;

    @Autowired
    private NodeStatistics nodeStatistics;

    public Controller() {
    }

    @Autowired
    public Controller(XMLNodeReader xmlNodeReader, NodeRepository repository) {
        this.xmlNodeReader = xmlNodeReader;
        this.repository = repository;
    }

    public void xmlToDatabase() throws JAXBException, XMLStreamException, SQLException {
        Node xmlNode;
        List<com.ivan.app.data.Node> nodesToBatch = new ArrayList<>();
        int nodeCount = 0;
        nodeStatistics = new NodeStatistics();
        logger.info("Start reading xml");
        while ((xmlNode = xmlNodeReader.getNextNode()) != null) {
            com.ivan.app.data.Node node = new com.ivan.app.data.Node(xmlNode);
            nodeStatistics.add(node);
            nodesToBatch.add(node);
            if (++nodeCount == BATCH_COUNT) {
                repository.save(nodesToBatch);
                nodesToBatch.clear();
                nodeCount = 0;
            }
        }
        printStatistic();
    }

    private void printStatistic() {
        nodeStatistics.getUsersStatistic().entrySet()
                .stream()
                .sorted((o1, o2) -> o2.getValue().compareTo(o1.getValue()))
                .forEach(entry -> System.out.println(entry.getKey() + " : " + entry.getValue()));
        System.out.println("***********************************");
        nodeStatistics.getKeysStatistic().entrySet()
                .stream()
                .sorted((o1, o2) -> o2.getValue().compareTo(o1.getValue()))
                .forEach(entry -> System.out.println(entry.getKey() + " : " + entry.getValue()));
    }
}
