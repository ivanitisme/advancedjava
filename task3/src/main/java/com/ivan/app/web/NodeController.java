package com.ivan.app.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ivan.app.data.Node;
import com.ivan.app.jpa.NodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by ivan on 6/3/16.
 */

@RestController
public class NodeController {
    @Autowired
    private NodeRepository repository;

    @RequestMapping("/node")
    public String getNodeInRadius(@RequestParam(value = "lat") double lat,
                                  @RequestParam(value = "lon") double lon,
                                  @RequestParam(value = "rad") double radius) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        List<Node> nodes = repository.getNodesInRadius(lat, lon, radius);
        return mapper.writeValueAsString(nodes.toArray());
    }
}
